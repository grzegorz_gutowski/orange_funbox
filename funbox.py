# vim:ts=4:sts=4:sw=4:expandtab
import json
import logging
import re
import requests

class Funbox:
    DEFAULT_ADDRESS = '192.168.1.1'

    def __init__(self, password, address=None):
        self.password = password
        self.address = address if address else Funbox.DEFAULT_ADDRESS
        self.cookies = None
        self.appuid = None
        self.context = None

    def set_cookie(self, key, value):
        self.cookies[self.appuid+'/'+key] = value

    def login(self):
        params=dict()
        params['username'] = 'admin'
        params['password'] = self.password
        r = requests.post('http://'+self.address+'/authenticate', params=params)
        self.cookies = r.cookies
        for key in self.cookies.keys():
            if re.match(r'.*/sessid', key):
                self.appuid = key.split('/')[0]
        self.context = json.loads(r.text)['data']['contextID']
        self.set_cookie('login', 'admin')
        self.set_cookie('context', self.context)

    def logout(self):
        r = requests.post('http://'+self.address+'/logout')
        self.cookies = None
        self.appuid = None
        self.context = None

    def __enter__(self):
        self.login()
        return self

    def __exit__(self, type, value, traceback):
        self.logout()

    @property
    def authenticated(self):
        return self.cookies is not None

    def ask(self, path, headers={}):
        if not self.authenticated:
            self.login()
        headers['X-Context'] = self.context
        r = requests.get('http://'+self.address+'/'+path, cookies=self.cookies, headers=headers)
        return r

    def call(self, path, params={}, headers={}):
        if not self.authenticated:
            self.login()
        headers['X-Context'] = self.context
        headers['Content-type'] = 'application/x-sah-ws-1-call+json; charset=UTF-8'
        r = requests.post('http://'+self.address+'/'+path, cookies=self.cookies, headers=headers, data=json.dumps({'parameters' : params}))
        return r 

    def sysbus(self, function, params={}):
        call = self.call('sysbus/'+function, params)
        result = json.loads(call.text)
        status = result.get('result', {}).get('status', False)
        if not status:
            logging.error(call.text)
        return result.get('result', {})

    def lan_devices(self):
        return self.sysbus('Hosts:getDevices').get('status', {})

    def device_info(self):
        info = self.ask('sysbus/DeviceInfo?_restDepth=-1')
        return dict([(param['name'], param['value']) for param in json.loads(info.text).get('parameters', []) if 'name' in param and 'value' in param])

    def network_info(self):
        info = dict()
        info['dsl'] = self.sysbus('NeMo/Intf/dsl0:getDSLStats').get('status', {})
        info['data'] = self.sysbus('NeMo/Intf/data:getMIBs').get('status', {})
        info['lan'] = self.sysbus('NeMo/Intf/lan:getMIBs').get('status', {})
        return info

    def simple_info(self):
        simple = dict()
        info = self.network_info()
        dev = self.device_info()
        simple['uptime'] = dev['UpTime']

        dsl_data = info['data']['dsl'].values()[0]
        #simple['dsl'] = dsl_data 
        simple['dsl_downstream'] = dsl_data['DownstreamCurrRate']
        simple['dsl_upstream'] = dsl_data['UpstreamCurrRate']
        simple['dsl_link'] = dsl_data['LinkStatus'].lower()
        simple['dsl_errors'] = dict([(k[0:-6],v) for k,v in info['dsl'].items() if k.endswith('Errors')])
        ppp_data = info['data']['ppp']['ppp_data']
        #simple['ppp'] = ppp_data
        simple['ppp_connection'] = ppp_data['ConnectionStatus'].lower()
        simple['ppp_user'] = ppp_data['Username']
        simple['ppp_ip'] = ppp_data['LocalIPAddress']
        simple['ppp_gate'] = ppp_data['RemoteIPAddress']
        simple['ppp_dns'] = ppp_data['DNSServers'].split(',')
        wifi_data = info['lan']['wlanvap'].values()[0]
        #simple['wifi'] = wifi_data
        simple['wifi_ssid'] = wifi_data['SSID']
        simple['wifi_password'] = wifi_data['Security']['KeyPassPhrase']
        return simple

    def reboot(self):
        call = self.call('sysbus/NMC:reboot')
        result = json.loads(call.text)
        status = result['result']['status']
        return status
